//
//  File.swift
//  
//
//  Created by Stefan Urbanek on 31/08/2022.
//

/*
 
 Required constraints:
 
 - UNIQUE: "Represents" outgoing
 - INVALID: "Represents" and "Indirect Origin"
 
 */

extension Edge {
    /// Create an edge from origin to target and marking it potentially
    /// as indirect depending on whether the endpoints are proxies or not.
    ///
    /// If `considerIndirection` is false, then this method creates a regular
    /// edge. If `considerIndirection` is true, then the edge will consider
    /// whether the origin and/or the target are proxies and will set the
    /// indirection labels accordingly:
    ///
    /// - If the origin is a proxy, then the edge will have ``IndirectionLabel.IndirectOrigin``
    ///   label set
    /// - If the target is a proxy, then the edge will have ``IndirectionLabel.IndirectTarget``
    ///   label set
    ///
    public convenience init(origin: Node,
                         target: Node,
                         labels: LabelSet=[],
                         dimension: DimensionTag = DefaultDimensionTag,
                         considerIndirection: Bool,
                         id: OID? = nil,
                         components: any Component...) {
        let additionalLabels: LabelSet

        if considerIndirection {
            if origin.isProxy && target.isProxy {
                additionalLabels = Set([IndirectionLabel.IndirectOrigin, IndirectionLabel.IndirectTarget])
            }
            else if origin.isProxy {
                additionalLabels = Set([IndirectionLabel.IndirectOrigin])
            }
            else if target.isProxy {
                additionalLabels = Set([IndirectionLabel.IndirectTarget])
            }
            else {
                additionalLabels = Set()
            }
        }
        else {
            additionalLabels = Set()
        }

        self.init(origin: origin.id,
                  target: target.id,
                  labels: labels.union(additionalLabels),
                  dimension: dimension,
                  id: id,
                  components: components)
    }
    
    /// Create an edge from a proxy object to its subject.
    ///
    /// This is a convenience method.
    ///
    /// - Note: This method is not checking for potential cycles as it has
    ///         no means to do it. It is in the caller's responsibility to check
    ///         for cycles.
    /// - Note: This method is not checking whether there are other subjects
    ///         connected to the provided proxy as it has no means to do it. It
    ///         is in the caller's responsibility to check whether the edge
    ///         is unique for the proxy.
    ///
    public convenience init(proxy: Node,
                         representing subject: Node,
                         labels: LabelSet=[],
                         dimension: DimensionTag = DefaultDimensionTag,
                         id: OID? = nil,
                         components: any Component...) {

        precondition(proxy.isProxy)

        self.init(origin: proxy.id,
                  target: subject.id,
                  labels: labels.union([IndirectionLabel.Subject]),
                  dimension: dimension,
                  id: id,
                  components: components)
    }
}

// FIXME: Do we still need these?

extension GraphProtocol {
    /// List of all ports in the graph.
    ///
    public var proxies: [Node] {
        nodes.filter { $0.isProxy }
    }
    
    /// Get a path from a proxy node to the real subject. Real subject is a
    /// node that is referenced by a direct subject edge.
    ///
    /// The function follows all indirect edges from the provided proxy node
    /// until it finds a subject edge that direct.
    ///
    /// - Precondition: Node must be a proxy and indirection integrity must
    ///   be assured.
    ///
    public func realSubjectPath(_ proxy: Node) -> Path {
        // FIXME: Check for loops
        precondition(proxy.isProxy)
        
        var current = proxy
        let path = Path()
        
        while true {
            guard let edge = subjectEdge(current.id) else {
                break
            }
            
            // FIXME: Deal with this
            if edge.hasIndirectTarget {
                current = node(edge.target)!
                assert(!path.joins(current.id), "Path must not contain a loop")
            }
            path.append(edge)
            if !edge.hasIndirectTarget {
                break
            }
            
        }
        
        return path
    }

    /// Edge that is a representation of the proxy node.
    ///
    /// Representation edge is an outgoing edge from the proxy node
    /// which has a label ``IndirectionLabel/Subject``.
    ///
    public func subjectEdge(_ proxy: NodeID) -> Edge? {
        return self.outgoing(proxy).first { $0.isSubject }
    }
    /// A node that the port represents. This is a direct subject, not the
    /// real subject if the subject edge is indirect.
    ///
    /// To get the real subject use ``realSubjectPath()`` to get the
    /// path to the real subject traversing indirect subject edges.
    /// Target of the last edge, which can be retrieved using ``Path/target``,
    /// is the real subject.
    ///
    public func subject(_ proxy: NodeID) -> NodeID? {
        return subjectEdge(proxy)?.target
    }

}
