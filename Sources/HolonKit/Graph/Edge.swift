//
//  File.swift
//  
//
//  Created by Stefan Urbanek on 2021/10/20.
//


/// Edge represents a connection between two nodes.
///
/// The edges in the graph have an origin node and a target node associated
/// with it. The edges are oriented for convenience and for most likely use
/// cases. Despite most of the functionality might be using the orientation,
/// it does not prevent one to treat the edges as non-oriented.
///
public final class Edge: Object {
    // FIXME: Add PersistableEdge
    
    /// Origin node of the edge - a node from which the edge points from.
    ///
    public let origin: NodeID
    
    /// Target node of the edge - a node to which the edge points to.
    ///
    public let target: NodeID
    
    
    /// Create an edge between an origin node and a target node.
    ///
    /// Parameters:
    ///
    ///     - origin: ID of the origin node
    ///     - target: ID of the target node
    ///     - labels: Set of labels assigned to the edge
    ///     - dimension: Dimension tag of the edge
    ///     - id: Edge ID. See note below.
    ///     - components: a list of components added to the edge
    ///
    /// If `id` is not provided an arbitrary generated ID will be assigned. The
    /// assigned ID is guaranteed to be unique only within the set of
    /// assigned IDs within the same process. It is highly advised that the
    /// caller assigns an explicit ID of the edge. Use this generated ID only
    /// for testing purposes or if you want know that the ID will be assigned
    /// later, yet before added to the graph.
    ///
    /// - Note: It is in the responsibility of the caller to make sure that the
    ///         origin and target nodes exist in a graph when this edge is added
    ///         to the graph.
    ///
    public required init(origin: NodeID,
                         target: NodeID,
                         labels: LabelSet=[],
                         dimension: DimensionTag = DefaultDimensionTag,
                         id: OID? = nil,
                         components: [any Component]) {
        self.origin = origin
        self.target = target
        super.init(id: id, labels: labels, dimension: dimension, components: components)
    }

    public convenience init(origin: NodeID,
                         target: NodeID,
                         labels: LabelSet=[],
                         dimension: DimensionTag = DefaultDimensionTag,
                         id: OID? = nil,
                         components: any Component...) {

        self.init(origin: origin,
                  target: target,
                  labels: labels,
                  dimension: dimension,
                  id: id,
                  components: components)
    }
    
    
    public override var description: String {
        return "Edge(id: \(idDebugString), \(origin) -> \(target), labels: \(labels.sorted())), dim: \(dimensionTag)"
    }
}
